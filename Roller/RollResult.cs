﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Roller
{
    public class RollResult
    {
        public RollResult(Func<IList<int>, IEnumerable<int>> method)
        {
            Method = method;
        }

        public List<int> Rolls { get; set; } = new List<int>();

        public Func<IList<int>, IEnumerable<int>> Method { get; private set; }

        public int Result => Method(Rolls).Sum();

        public string DistinctRolls { get { return string.Join(" ", Rolls.OrderByDescending(x => x)); } }

        public string EffectiveRolls { get { return string.Join(" ", Method(Rolls).OrderByDescending(x => x)); } }

        public string Modifier
        {
            get
            {
                double modifier = (Result - 10) / 2.0;
                return string.Format("({0:+#;-#;+0})", (int)Math.Floor(modifier));
            }
        }
    }
}
