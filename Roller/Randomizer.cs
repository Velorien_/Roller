﻿using System;

namespace Roller
{
    public class Randomizer
    {
        private Random random = new Random();

        public int Roll(int min = 1, int max = 6)
        {
            random = new Random(random.Next());
            return random.Next(min, max + 1);
        }
    }
}
