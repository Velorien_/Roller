﻿using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Roller
{
    public class DynamicRoller
    {
        private static string codeBase = @"
            using System;
            using System.Collections.Generic;
            using System.Linq;

            namespace DynamicRoller
            {{
                public class DynamicClass
                {{
                    public static IEnumerable<int> Method(IList<int> x)
                    {{
                        {0}
                    }}
                }}
            }}";

        public static MethodInfo CreateMethod(string code)
        {
            string finalCode = string.Format(codeBase, code);
            var provider = new CSharpCodeProvider(new Dictionary<string, string> { { "CompilerVersion", "v3.5" } });
            var compilerParameters = new CompilerParameters()
            {
                GenerateExecutable = false,
                GenerateInMemory = true,
                IncludeDebugInformation = false
            };
            compilerParameters.ReferencedAssemblies.Add("System.Core.dll");
            var result = provider.CompileAssemblyFromSource(compilerParameters, finalCode);

            if(result.Errors.Count > 0)
            {
                var builder = new StringBuilder();
                foreach(var error in result.Errors)
                {
                    builder.AppendLine(error.ToString());
                }

                throw new Exception(builder.ToString());
            }

            Type type = result.CompiledAssembly.GetType("DynamicRoller.DynamicClass");

            return type.GetMethod("Method");
        }
    }
}
